#iOS目录

##[问答专区](http://192.168.1.63/thinkive_ios_Groups/Wikis/issues)  


##wiki 目录索引  

新添加的wiki都在thinkive_ios_Groups/WiKis 里添加  

添加的新wiki page的格式如下  

命名规则：[分类名]标题名  

**分类名**：现在暂时有如下分类，如果要添加的分类没有你要的，可以修改此ReadMe添加你的分类  

[帮助]  [个人分享]  [公共组件]  [代码规范]    
[基础框架]  [新闻动态]  [优化建议]  [成员简介]



##公共组件维护人员列表:  

分享插件：50230、50231    负责人：[叶璐](http://192.168.1.63/thinkive_ios_Groups/Wikis/wikis/%5B%E4%B8%AA%E4%BA%BA%E5%88%86%E4%BA%AB%5D%E5%BD%AD%E4%BC%98%E7%A7%80%E4%B8%AA%E4%BA%BA%E7%AE%80%E4%BB%8B)  

统一登录插件：            负责人：[康铭](http://192.168.1.63/thinkive_ios_Groups/Wikis/wikis/%5B%E4%B8%AA%E4%BA%BA%E5%88%86%E4%BA%AB%5D%E5%BD%AD%E4%BC%98%E7%A7%80%E4%B8%AA%E4%BA%BA%E7%AE%80%E4%BB%8B)    


启动引导页                负责人: [王创](http://192.168.1.63/thinkive_ios_Groups/Wikis/wikis/%5B%E4%B8%AA%E4%BA%BA%E5%88%86%E4%BA%AB%5D%E5%BD%AD%E4%BC%98%E7%A7%80%E4%B8%AA%E4%BA%BA%E7%AE%80%E4%BB%8B)  

H5 检测更新               负责人: [彭广](http://192.168.1.63/thinkive_ios_Groups/Wikis/wikis/%5B%E4%B8%AA%E4%BA%BA%E5%88%86%E4%BA%AB%5D%E5%BD%AD%E4%BC%98%E7%A7%80%E4%B8%AA%E4%BA%BA%E7%AE%80%E4%BB%8B)
